--[[
Modified Event library

Originally from https://github.com/Afforess/Factorio-Stdlib
Merged the 'fail_if_missing' function from stdlib's core

This has been modified to allow for string-based event names which
start with the text "custom."  This allows for registering & dispatching
events such as "custom.rankChanged." - Kovus

--]]

local function fail_if_missing(var, msg)
    if not var then
        error(msg or "Missing value", 3)
    end
    return false
end

Event = { --luacheck: allow defined top
    _registry = {},
    core_events = {
        init = -1,
        load = -2,
        configuration_changed = -3,
        _register = function(id)
            if id == Event.core_events.init then
                script.on_init(
                    function()
                        Event.dispatch({name = Event.core_events.init, tick = game.tick})
                    end
                )
            elseif id == Event.core_events.load then
                script.on_load(
                    function()
                        Event.dispatch({name = Event.core_events.load, tick = -1})
                    end
                )
            elseif id == Event.core_events.configuration_changed then
                script.on_configuration_changed(
                    function(event)
                        event.name = Event.core_events.configuration_changed
                        event.data = event -- for backwards compatibilty
                        Event.dispatch(event)
                    end
                )
            end
        end
    }
}

--- Registers a function for a given event.
-- Events are dispatched in the order they are registered.
-- @usage Event.register(defines.events.on_tick, function(event) print event.tick end)
-- -- creates an event that prints the current tick every tick.
-- @tparam defines.events|{defines.events,...} event events to register
-- @tparam function handler Function to call when event is triggered
-- @treturn Event
function Event.register(event, handler)
    fail_if_missing(event, "missing event argument")
	local custom_event = false

    if type(event) == "number" then
        event = {event}
    end
	if type(event) == "string" then
		if string.sub(event, 1, string.len('custom.')) == 'custom.' then
			custom_event = true
		end
		event = {event}
	end

    for _, event_id in pairs(event) do
        fail_if_missing(event_id, "missing event id")
        if handler == nil then
            Event._registry[event_id] = nil
			if not custom_event then
	            script.on_event(event_id, nil)
			end
        else
            if not Event._registry[event_id] then
                Event._registry[event_id] = {}

                if type(event_id) == "string" or event_id >= 0 then
					if not custom_event then
	                    script.on_event(event_id, Event.dispatch)
					end
                else
                    Event.core_events._register(event_id)
                end
            end
            table.insert(Event._registry[event_id], handler)
        end
    end
    return Event
end

--- Calls the registerd handlers
-- Will stop dispatching remaning handlers if any handler passes invalid event data.
-- Handlers are dispatched in the order they were created
-- @tparam table event LuaEvent as created by script.raise_event
-- @see https://forums.factorio.com/viewtopic.php?t=32039#p202158 Invalid Event Objects
function Event.dispatch(event)
    if event then
        local name = event.input_name or event.name

        if Event._registry[name] then
            local force_crc = Event.force_crc

            for idx, handler in ipairs(Event._registry[name]) do

                -- Check for userdata and stop processing further handlers if not valid
                for _, val in pairs(event) do
                    if type(val) == "table" and val.__self == "userdata" and not val.valid then
                        return
                    end
                end

                local metatbl = { __index = function(tbl, key) if key == '_handler' then return handler else return rawget(tbl, key) end end }
                setmetatable(event, metatbl)

                -- Call the handler
                local success, err = pcall(handler, event)

                -- If the handler errors lets make sure someone notices
                if not success then
                  	error(err) -- no players received the message, force a real error so someone notices
                end

                -- force a crc check if option is enabled. This is a debug option and will hamper perfomance if enabled
                if (force_crc or event.force_crc) and _G.game then
                    local msg = 'CRC check called for event '..event.name..' handler #'..idx
                    log(msg)  -- log the message to factorio-current.log
                    game.force_crc()
                end

                -- if present stop further handlers for this event
                if event.stop_processing then
                    return
                end
            end
        end
    else
        error('missing event argument')
    end
end

--- Removes the handler from the event
-- @tparam defines.events|{defines.events,...} event events to remove the handler for
-- @tparam function handler to remove
-- @return Event
function Event.remove(event, handler)
    fail_if_missing(event, "missing event argument")
    fail_if_missing(handler, "missing handler argument")

	local custom_event = false

    if type(event) == "number" then
        event = {event}
    end
	if type(event) == "string" then
		if string.sub(event, 1, string.len('custom.')) == 'custom.' then
			custom_event = true
		end
		event = {event}
	end

    for _, event_id in pairs(event) do
        fail_if_missing(event_id, "missing event id")
        if Event._registry[event_id] then
            for i=#Event._registry[event_id], 1, -1 do
                if Event._registry[event_id][i] == handler then
                    table.remove(Event._registry[event_id], i)
                end
            end
            if #Event._registry[event_id] == 0 then
                Event._registry[event_id] = nil
				if not custom_event then
	                script.on_event(event_id, nil)
				end
            end
        end
    end
    return Event
end

return Event
