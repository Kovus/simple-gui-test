require 'event'
require 'mod-gui'

function createButton(player)
	local frame = mod_gui.get_button_flow(player)
	if not frame.btn_toggle_test1 then
		frame.add({
			name = "btn_toggle_test1", type = "sprite-button", caption="Test", 
			tooltip="Just a quick GUI test", 
			style=mod_gui.button_style
		})
	end
end

function createGUI(player)
	local flow = player.gui.center
	local frame = flow.add({
		type = "frame", name = "testgui", caption = 'Test GUI', 
		style = 'frame_style', direction = 'vertical',
	})
	frame.add({type = 'button', name = 'btn_test_msg', caption = 'Message'})
	frame.add({type = 'label', caption = 'Cur Tick: '..game.tick})
end

function removeGUI(player)
	local frame = player.gui.center.testgui
	if frame then
		frame.destroy()
	end
end

Event.register(defines.events.on_player_joined_game, function(event)
	local player = game.players[event.player_index]
	createButton(player)
end)

-- handle on-gui-click events which map to provided callbacks.
Event.register(defines.events.on_gui_click, function(event)
	if not (event and event.element and event.element.valid) then
		return
	end
	
	local player = game.players[event.player_index]
	if event.element.name == 'btn_toggle_test1' then
		player.print("Clicked button on tick: "..event.tick)
		if player.gui.center.testgui then
			removeGUI(player)
		else
			createGUI(player)
		end
	end
	if event.element.name == 'btn_test_msg' then
		game.print(player.name .. " clicked the button")
	end
end)

