require 'util' -- Factorio-provided
require "builtin-silo"
require "builtin-playerspawn"

require 'event'

require 'gui-test1'

local version = 1

Event.register(Event.core_events.init, function()
	global.version = version
end)

Event.register(Event.core_events.configuration_changed, function(event)
	if global.version ~= version then
		global.version = version
	end
end)
